import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:information_app/view/client/view.dart';
import 'package:information_app/view/client/form.dart';
import 'package:information_app/view/help.dart';
import 'package:information_app/view/pdf.dart';
import 'package:information_app/view/user/alert.dart';
import 'package:information_app/view/user/login.dart';
import 'package:information_app/view/user/view.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Information app',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: "/",
      routes: {
        "/": (context) => MyHomePage(title: 'Information app'),
        "/user": (context) => UserView(),
        "/user/login": (context) => UserLogin(),
        "/client": (context) => ClientView(),
        "/client/form": (context) => ClientForm(),
        "/help": (context) => HelpView(),
        "/alert": (context) => AlertView(),
        "/pdf": (context) => PdfView(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  void changeRoute(String routeName) {
    Navigator.pushNamed(context, '/' + routeName);
  }

  Future<String> getDeviceId() async {
    DeviceInfoPlugin deviceInfo = new DeviceInfoPlugin();
    var aa = await deviceInfo.androidInfo;
    return aa.androidId;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.help_outline,
              color: Colors.white,
            ),
            onPressed: () {
              changeRoute("help");
            },
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/bar.jpg"),
                    fit: BoxFit.cover,
                  ),
                ),
                child: SizedBox.expand(
                  child: Opacity(
                    opacity: 0.7,
                    child: barButton(),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/client.jpeg"),
                    fit: BoxFit.cover,
                  ),
                ),
                child: SizedBox.expand(
                  child: Opacity(
                    opacity: 0.7,
                    child: clientButton(),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  MaterialButton clientButton() {
    return MaterialButton(
      padding: EdgeInsets.all(8.0),
      textColor: Colors.white,
      splashColor: Colors.blueAccent,
      elevation: 8.0,
      child: Container(
        child: Text(
          "CLIENTE",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 45,
            letterSpacing: -3,
          ),
        ),
      ),
      onPressed: () {
        changeRoute("client");
      },
    );
  }

  MaterialButton barButton() {
    return MaterialButton(
      padding: EdgeInsets.all(8.0),
      textColor: Colors.white,
      splashColor: Colors.blueAccent,
      elevation: 8.0,
      child: Container(
        child: Text(
          "ESTABLECIMIENTO",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 45,
            letterSpacing: -3,
          ),
        ),
      ),
      onPressed: () async {
        changeRoute("user");
      },
    );
  }
}
