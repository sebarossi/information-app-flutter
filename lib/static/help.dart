const GENERAL_EXPLAINATION =
    "Esta aplicación tiene como propósito agilizar la recolección de datos " +
        "personales por parte de los establecimientos que lo requieran, de forma " +
        "que no sea necesario requerir a sus clientes el llenado de una planilla" +
        "\n Para lograr esto, se pedirá que los usuarios de la aplicación " +
        "(clientes) llenen sus datos por única vez, y podrán transferirlos a " +
        "cada establecimiento sin demora ni contacto, mediante el uso de un " +
        "código QR";

const DATA_COLLECTION =
    "La recolección de datos requiere que cada usuario complete sólo los datos " +
        "personales relevantes para el establecimiento, los cuales le son " +
        "requeridos por las normativas vigentes.\n ";

const ALERT_NOTIFICATIONS =
    "Llegado el caso de que en un establecimiento se detecte un usuario que " +
        "haya presentado síntomas de COVID-19, dicho establecimiento tiene " +
        "la posibilidad de informar a los clientes afectados mediante la " +
        "aplicación. Esta opción le permitirá seleccionar la fecha en que el " +
        "cliente visitó el establecimiento, y enviar una notificación " +
        "automática a los demás clientes que hayan visitado el establecimiento " +
        "en los 7 días próximos. \n" +
        "Opcionalmente, el establecimiento podrá incluir un mensaje adicional " +
        "en la notificación, para dar mejor información a los clientes afectados.";

const CLIENTS_PRIVACY = "Todos los datos recolectados de los clientes son " +
    "mantenidos internamente, con el objetivo de proporcionar información " +
    "adicional a las autoridades, en caso de que le sea requerido al " +
    "establecimiento. Ninguno de los datos ingresados por el usuario es público " +
    "ni visible para los establecimientos. Cuando al establecimiento le sean " +
    "requeridos los datos de uno o más de sus clientes, deberá comunicarse con " +
    "el soporte de esta aplicación, para hacer el pedido de los mismos.\n " +
    "Este pedido debe estar acompañado de solicitud de datos realizada por las " +
    "autoridades pertirnentes. \n " +
    "Cualquier otro pedido de datos por parte de establecimientos será " +
    "denegado, con el objetivo de mantener los datos de los clientes de forma " +
    "privada y segura";

const DATA_DELETEMENT =
    "En cualquier momento, un cliente podrá solicitar la eliminación de sus " +
        "datos de la base de datos de la aplicación. Esta acción no dejará " +
        "ningún registro sobre los datos personales del cliente en la base de " +
        "datos.\n " +
        "Luego del borrado de datos, el cliente podrá volver a cargar los " +
        "mismos y continuar con el uso de la aplicación";
