import 'base_model.dart';

class Event extends BaseModel {
  final int userId;
  final String informationId;
  final DateTime date;
  Event({this.informationId, this.userId, this.date});
}
