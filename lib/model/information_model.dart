import 'base_model.dart';

class Information extends BaseModel {
  final String deviceId;
  final String name;
  final String lastName;
  final String document;
  final String address;

  Information(
      {this.deviceId, this.name, this.lastName, this.document, this.address});
}
