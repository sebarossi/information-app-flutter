// import 'package:fitness_app/models/events.dart';
// import 'package:fitness_app/models/locations.dart';
// import 'package:fitness_app/models/reservations.dart';
// import 'package:fitness_app/models/users.dart';

import 'user_model.dart';
import 'event_model.dart';
import 'information_model.dart';

class BaseModel {
  static oneFromJson<X>(Map<String, dynamic> json) {
    switch (X) {
      case User:
        return User(
          id: json["id"],
          email: json["email"],
          password: json["password"],
          name: json["name"],
          address: json["address"],
          carta: json["carta"],
        );
        break;

      case Information:
        return Information(
          deviceId: json["device_id"],
          name: json["name"],
          lastName: json["lastName"],
          document: json["document"],
          address: json["address"],
        );
        break;

      case Event:
        return Event(
          informationId: json["information_id"],
          userId: json["user_id"],
          date: DateTime.parse(json["date"]),
        );
        break;

      default:
        return null;
    }
  }

  static List<X> listFromJson<X>(List<dynamic> json) {
    List<X> list = new List<X>();
    for (var entity in json) {
      list.add(BaseModel.oneFromJson<X>(entity));
    }
    return list;
  }
}
