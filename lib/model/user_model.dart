import 'base_model.dart';

class User extends BaseModel {
  final int id;
  String email, password, name, address, carta;
  User({this.id, this.email, this.password, this.name, this.address, this.carta});
}
