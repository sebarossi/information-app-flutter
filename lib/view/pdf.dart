import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:information_app/adapter/user_adapter.dart';
import 'package:information_app/index.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';

class PdfView extends StatelessWidget {
  String filePath;

  @override
  Widget build(BuildContext context) {
    this.filePath = ModalRoute.of(context).settings.arguments;
    if (this.filePath != null && this.filePath != "") {
      return Scaffold(
          appBar: AppBar(
            title: Text("Carta"),
          ),
          body: FutureBuilder(
            future: this.loadPdf(),
            builder: ((context, snapshot) {
              if (snapshot.hasData) {
                return PDFViewer(document: snapshot.data);
              }
              return Center(
                child: CircularProgressIndicator(),
              );
            }),
          ));
    } else {
      Navigator.pop(context);
      return Spacer();
    }
  }

  void goBack(context) {
    Navigator.pop(context);
  }

  Future<PDFDocument> loadPdf() async {
    PDFDocument doc =
        await PDFDocument.fromFile(await UserAdapter.getFile(this.filePath));
    return doc;
  }
}
