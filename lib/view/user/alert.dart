import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:information_app/adapter/event_adapter.dart';

class AlertView extends StatefulWidget {
  @override
  _AlertViewState createState() => _AlertViewState();
}

class _AlertViewState extends State<AlertView> {
  DateTime _alertDate;
  String _alertMessage;
  int _userId;
  final _formKey = new GlobalKey<FormState>();

  void goBack(context, newInfo) {
    Navigator.pop(context, newInfo);
  }

  bool canSubmit() {
    if (this._formKey.currentState.validate()) {
      return true;
    }
    return false;
  }

  void createAlert(buttonContext) async {
    Navigator.pop(context);
    this._userId = ModalRoute.of(context).settings.arguments;
    String message;
    try {
      Map<String, dynamic> alertResponse = await EventAdapter.sendAlert(
        this._userId,
        this._alertDate,
        "Un establecimiento envió una alerta",
        this._alertMessage,
      );
      if (alertResponse != null) {
        int successCount = alertResponse["successCount"];
        int errorCount = alertResponse["failureCount"];
        if (successCount == 1) {
          message = "1 cliente fue notificado.";
        } else {
          message = successCount.toString() + " clientes fueron notificados.";
        }
        if (errorCount > 0) {
          if (errorCount == 1) message += " (1 no fue notificado).";
          message += " (" + errorCount.toString() + " no fueron notificados).";
        }
      } else {
        message = "No se pudo enviar la notificación";
      }
    } catch (ex) {
      message = ex.toString();
    }
    Scaffold.of(buttonContext).showSnackBar(
      SnackBar(
        content: Text(message),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Alertar a clientes"),
      ),
      body: form(),
    );
  }

  Form form() {
    return Form(
      key: this._formKey,
      child: SingleChildScrollView(
        child: Column(
          children: [
            dateInput(),
            messageInput(),
            submitBuilder(),
          ],
        ),
      ),
    );
  }

  Builder submitBuilder() {
    return Builder(
      builder: (context) => ElevatedButton(
        child: Text("Submit"),
        onPressed: () async {
          if (this.canSubmit()) {
            List<Widget> actions = [
              FlatButton(
                child: Text("Cancelar"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                child: Text("Enviar"),
                onPressed: () {
                  this.createAlert(context);
                },
              ),
            ];
            alertDialog(
              "Confirmar alerta?",
              "Se enviará una notificación a los clientes afectados",
              actions,
            );
          }
        },
      ),
    );
  }

  Container messageInput() {
    return Container(
      child: TextFormField(
        maxLines: 10,
        keyboardType: TextInputType.multiline,
        autofocus: false,
        decoration: new InputDecoration(
          hintText: "Mensaje a los clientes (opcional)",
        ),
        onChanged: (value) => setState(() {
          this._alertMessage = value.trim();
        }),
        textInputAction: TextInputAction.next,
      ),
    );
  }

  Future<void> _selectDate(BuildContext context) async {
    DateTime initialTime = this._alertDate != null ? this._alertDate : DateTime.now();
    final DateTime datePicked = await showDatePicker(
      context: context,
      initialDate: initialTime,
      firstDate: DateTime(2015, 8),
      lastDate: DateTime.now(),
    );
    final TimeOfDay timePicked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );
    if (datePicked != null && timePicked != null) {
      setState(() {
        this._alertDate =
            DateTime(datePicked.year, datePicked.month, datePicked.day, timePicked.hour, timePicked.minute);
      });
    }
  }

  Container dateInput() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            this._alertDate != null ? this._alertDate.toString() : "Selecciones una fecha",
          ),
          ElevatedButton(
              onPressed: () => _selectDate(context),
              child: Icon(
                Icons.edit,
              )),
        ],
      ),
    );
  }

  void alertDialog(String title, String content, List<Widget> actions) {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: Text(title),
        content: Text(content),
        actions: actions,
      ),
    );
  }
}
