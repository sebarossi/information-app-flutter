import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:information_app/adapter/user_adapter.dart';
import 'package:information_app/index.dart';
import 'package:information_app/model/user_model.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:file_picker/file_picker.dart';

class UserView extends StatefulWidget {
  @override
  _UserViewState createState() => _UserViewState();
}

class _UserViewState extends State<UserView> {
  QrImage _qrCode;
  User _userModel;

  @override
  void initState() {
    super.initState();
  }

  void goToAlert() {
    Navigator.pushNamed(
      context,
      '/alert',
      arguments: this._userModel.id,
    );
  }

  void goBack(context) {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Establecimiento"),
      ),
      body: Center(
        child: FutureBuilder(
          future: this.setUserModel(),
          builder: this.homePageBuilder,
        ),
      ),
    );
  }

  Widget homePageBuilder(context, snapshot) {
    if (snapshot.hasData) {
      if (this._userModel != null) {
        return homePage(context);
      } else {
        // Se hace de esta manera para poder cambiar de ruta durante build
        @override
        void run() {
          scheduleMicrotask(() {
            Navigator.pushReplacementNamed(context, '/user/login');
          });
        }

        run();
        // Este progress indicator se carga mientras se procesa la microtask
        return CircularProgressIndicator();
      }
    } else {
      return CircularProgressIndicator();
    }
  }

  Widget homePage(BuildContext context) {
    return Container(
      child: Column(
        children: [
          ElevatedButton(
            child: Text("Compartir código QR"),
            onPressed: generateQrCode,
          ),
          qrCode(),
          ElevatedButton(
            child: Text("Subir una carta"),
            onPressed: this.uploadFile,
          ),
          ElevatedButton(
            child: Text("Notificar un cliente infectado"),
            onPressed: this.goToAlert,
          ),
        ],
      ),
    );
  }

  Future<bool> setUserModel() async {
    Map<String, dynamic> userData;
    User user;
    try {
      userData = await UserAdapter.getInfo();
      if (userData["login"] && userData["user"] != null) {
        user = userData["user"];
        this._userModel = user;
        return true;
      }
    } catch (e) {
      // algo con un error
    }
    return false;
  }

  void generateQrCode() {
    Map<String, dynamic> qrData = {
      "establishment_id": this._userModel.id,
      "qr_date": DateTime.now().toString()
    };
    String qrString = jsonEncode(qrData);
    QrImage qrCode = QrImage(
      data: qrString,
      version: QrVersions.auto,
      size: 200.0,
    );
    setState(() {
      this._qrCode = qrCode;
    });
  }

  Padding qrCode() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: this._qrCode,
    );
  }

  alertDialog(context, String title, String content) {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: Text(title),
        content: Text(content),
        actions: [
          FlatButton(
            child: Text("Ok"),
            onPressed: () {
              Navigator.pop(context, false);
            },
          ),
        ],
      ),
    );
  }

  qrDialog(context, String title, QrImage content) {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: Text(title),
        content: Center(
          child: content,
        ),
        actions: [
          FlatButton(
            child: Text("Ok"),
            onPressed: () {
              Navigator.pop(context, false);
            },
          ),
        ],
      ),
    );
  }

  uploadFile() async {
    FilePickerResult result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['pdf'],
    );

    if (result != null) {
      File file = File(result.files.single.path);
      String message;
      try {
        String filePath = await UserAdapter.upload(this._userModel.id, file);
        message = "Archivo subido correctamente";
      } catch (e) {
        message = e;
      }
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: Text(message),
          actions: [
            FlatButton(
              child: Text("Ok"),
              onPressed: () {
                Navigator.pop(context, false);
              },
            ),
          ],
        ),
      );
    } else {
      // User canceled the picker
    }
  }
}
