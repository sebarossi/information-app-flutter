import 'package:flutter/material.dart';
import 'package:information_app/adapter/user_adapter.dart';
import 'package:information_app/index.dart';
import 'package:flutter/widgets.dart';

final storage = FlutterSecureStorage();

class UserLogin extends StatefulWidget {
  @override
  _UserLoginState createState() => _UserLoginState();
}

class _UserLoginState extends State<UserLogin> {
  String _email, _password;
  final _formKey = new GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  void changeRoute(String routeName) {
    Navigator.pushNamed(
      context,
      '/' + routeName,
    ).then((newInformationModel) {
      setState(() {});
    });
  }

  void goBack(context) {
    Navigator.pop(context);
  }

  void processForm(formContext) {
    if (this.canSubmit()) {
      this.saveForm();
      makeLogin(formContext);
    }
  }

  bool canSubmit() {
    if (this._formKey.currentState.validate()) {
      return true;
    }
    return false;
  }

  void saveForm() {
    this._formKey.currentState.save();
  }

  makeLogin(formContext) async {
    Map<String, String> loginMap = {
      "email": this._email,
      "password": this._password
    };
    try {
      var userData = await UserAdapter.login(loginMap);
      if (userData != null && userData["token"] != null) {
        storage.write(key: "jwt", value: userData["token"]);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.setBool('isLoggedIn', true);
        Navigator.pushReplacementNamed(context, '/user');
      } else {
        throw new Exception(
            "No account was found matching that username and password");
      }
    } catch (e) {
      Scaffold.of(formContext).showSnackBar(
        SnackBar(
          content: Text("Hubo un error, inténtelo más tarde"),
        ),
      );
    }
  }

  Widget showEmailInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
          hintText: "Email",
          icon: new Icon(
            Icons.mail,
            color: Colors.grey,
          ),
        ),
        validator: (value) => value.isEmpty ? "Este campo es requerido" : null,
        onSaved: (value) => this._email = value.trim(),
        textInputAction: TextInputAction.next,
      ),
    );
  }

  Widget showPasswordInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        maxLines: 1,
        obscureText: true,
        keyboardType: TextInputType.visiblePassword,
        autofocus: false,
        decoration: new InputDecoration(
          hintText: "Password",
          icon: new Icon(
            Icons.lock,
            color: Colors.grey,
          ),
        ),
        validator: (value) => value.isEmpty ? "Este campo es requerido" : null,
        onSaved: (value) => _password = value.trim(),
        textInputAction: TextInputAction.done,
      ),
    );
  }

  Widget showPrimaryButton(buildContext) {
    return new Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
          height: 40.0,
          child: new RaisedButton(
            elevation: 5.0,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            color: Colors.blue,
            child: new Text("Loguear",
                style: new TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                )),
            onPressed: () {
              processForm(buildContext);
            },
          )),
    );
  }

  Widget showForm(buildContext) {
    return new Container(
      padding: EdgeInsets.all(16.0),
      child: new Form(
        key: _formKey,
        child: new ListView(
          shrinkWrap: true,
          children: <Widget>[
            showEmailInput(),
            showPasswordInput(),
            showPrimaryButton(buildContext),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Establecimiento - Login"),
      ),
      body: Builder(
        builder: (context) => Center(
          child: Stack(
            children: <Widget>[
              showForm(context),
            ],
          ),
        ),
      ),
    );
  }

  alertDialog(context, String title, String content) {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: Text(title),
        content: Text(content),
        actions: [
          FlatButton(
            child: Text("Ok"),
            onPressed: () {
              Navigator.pop(context, false);
            },
          ),
        ],
      ),
    );
  }
}
