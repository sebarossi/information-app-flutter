import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:information_app/adapter/information_adapter.dart';
import 'package:information_app/model/information_model.dart';
import 'package:information_app/view/client/view.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

class ClientForm extends StatefulWidget {
  @override
  _ClientFormState createState() => _ClientFormState();
}

class _ClientFormState extends State<ClientForm> {
  String _deviceId;
  String _name, _lastName, _address;
  int _phoneNumber, _document;
  DateTime _birthday;
  PickedFile _image;
  final _formKey = new GlobalKey<FormState>();
  DeviceInfo args = null;

  void goBack(context, newInfo) {
    Navigator.pop(context, newInfo);
  }

  bool canSubmit() {
    if (this._formKey.currentState.validate()) {
      return true;
    }
    return false;
  }

  void createInformation(buttonContext) async {
    Map<String, String> newInformationMap = {
      "device_id": this._deviceId,
      "name": this._name,
      "lastName": this._lastName,
      "document": this._document.toString(),
      "address": this._address,
    };
    try {
      String token = await _firebaseMessaging.getToken();
      newInformationMap["fcm_token"] = token;
      Information newInformation =
          await InformationAdapter.create(newInformationMap);
      if (newInformation != null) {
        this.goBack(context, newInformation);
      } else {
        Scaffold.of(buttonContext).showSnackBar(
          SnackBar(
            content: Text("Hubo un error, inténtelo más tarde"),
          ),
        );
      }
    } catch (ex) {
      Scaffold.of(buttonContext).showSnackBar(
        SnackBar(
          content: Text("Hubo un error, inténtelo más tarde"),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    args = ModalRoute.of(context).settings.arguments;
    this._deviceId = args.deviceId;
    return Scaffold(
      appBar: AppBar(
        title: Text("Cliente - Ingreso de datos"),
      ),
      body: form(),
    );
  }

  Form form() {
    return Form(
      key: this._formKey,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: Text("Campos requeridos:"),
            ),
            nameInput(),
            lastNameInput(),
            phoneInput(),
            documentInput(),
            addressInput(),
            birthdayInput(),
            Center(
              child: Text("Campos opcionales:"),
            ),
            Builder(
              builder: (context) => ElevatedButton(
                child: Text("Submit"),
                onPressed: () => {
                  if (this.canSubmit())
                    {
                      createInformation(context),
                    }
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Container nameInput() {
    return Container(
      child: TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.name,
        autofocus: false,
        decoration: new InputDecoration(
          hintText: "Nombre",
        ),
        validator: (value) => value.isEmpty ? "Campo requerido" : null,
        onChanged: (value) => setState(() {
          this._name = value.trim();
        }),
        textInputAction: TextInputAction.next,
      ),
    );
  }

  Container lastNameInput() {
    return Container(
      child: TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.name,
        autofocus: false,
        decoration: new InputDecoration(
          hintText: "Apellido",
        ),
        validator: (value) => value.isEmpty ? "Campo requerido" : null,
        onChanged: (value) => setState(() {
          this._lastName = value.trim();
        }),
        textInputAction: TextInputAction.next,
      ),
    );
  }

  Container phoneInput() {
    return Container(
      child: TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.phone,
        autofocus: false,
        decoration: new InputDecoration(
          hintText: "Teléfono",
        ),
        validator: (value) => value.isEmpty ? "Campo requerido" : null,
        onChanged: (value) => setState(() {
          this._phoneNumber = int.parse(value);
        }),
        textInputAction: TextInputAction.next,
      ),
    );
  }

  Container documentInput() {
    return Container(
      child: TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.number,
        autofocus: false,
        decoration: new InputDecoration(
          hintText: "Documento",
        ),
        validator: (value) => value.isEmpty ? "Campo requerido" : null,
        onChanged: (value) => setState(() {
          this._document = int.parse(value);
        }),
        textInputAction: TextInputAction.next,
      ),
    );
  }

  Container addressInput() {
    return Container(
      child: TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.streetAddress,
        autofocus: false,
        decoration: new InputDecoration(
          hintText: "Dirección",
        ),
        validator: (value) => value.isEmpty ? "Campo requerido" : null,
        onChanged: (value) => setState(() {
          this._address = value.trim();
        }),
        textInputAction: TextInputAction.next,
      ),
    );
  }

  Container birthdayInput() {
    return Container(
      child: InputDatePickerFormField(
        autofocus: false,
        firstDate: new DateTime(1930),
        lastDate: new DateTime(2020),
        onDateSaved: (value) => setState(() {
          this._birthday = value;
        }),
      ),
    );
  }

  AlertDialog alertDialog() {
    return AlertDialog(
      title: Text("SUBMIT PERRI"),
      actions: [
        FlatButton(
          child: Text("Ok"),
          onPressed: () {},
        ),
      ],
    );
  }
}
