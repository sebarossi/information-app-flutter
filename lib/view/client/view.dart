import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:device_info/device_info.dart';
import 'package:information_app/adapter/event_adapter.dart';
import 'package:information_app/adapter/information_adapter.dart';
import 'package:information_app/adapter/user_adapter.dart';
import 'package:information_app/index.dart';
import 'package:information_app/model/event_model.dart';
import 'package:information_app/model/information_model.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:information_app/model/user_model.dart';

final storage = FlutterSecureStorage();

class ClientView extends StatefulWidget {
  @override
  _ClientViewState createState() => _ClientViewState();
}

class _ClientViewState extends State<ClientView> {
  Information informationModel;
  String _deviceId;
  User _currentLocation;
  DateTime _arrivalDate;

  @override
  void initState() {
    super.initState();
  }

  void changeToForm() {
    Navigator.pushNamed(
      context,
      '/client/form',
      arguments: DeviceInfo(this._deviceId),
    ).then((newInformationModel) {
      setState(() {
        this.informationModel = newInformationModel != null ? newInformationModel : null;
      });
    });
  }

  void changeRoute(String routeName, {arguments}) {
    Navigator.pushNamed(context, "/" + routeName, arguments: arguments);
  }

  void goBack(context) {
    Navigator.pop(context);
  }

  Future<void> setDeviceId() async {
    DeviceInfoPlugin deviceInfo = new DeviceInfoPlugin();
    var aa = await deviceInfo.androidInfo;
    this._deviceId = aa.androidId;
  }

  Future<bool> setInformationModel() async {
    Information info;
    try {
      await this.setDeviceId();
      info = await InformationAdapter.getOneInformation(this._deviceId);
      if (info != null) {
        this.informationModel = info;
        return true;
      }
    } catch (e) {
      // algo con un error
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cliente"),
      ),
      body: Center(
        // child: homePage(context),
        child: FutureBuilder(
          future: this.setInformationModel(),
          builder: homePageBuilder,
        ),
      ),
      floatingActionButton: Builder(
        builder: (context) => fab(context),
      ),
    );
  }

  Widget homePageBuilder(BuildContext context, snapshot) {
    if (snapshot.hasData) {
      if (this.informationModel != null) {
        return Center(
          child: Container(
            child: Column(
              children: [
                Icon(
                  Icons.account_circle,
                  color: Colors.green[200],
                  size: 200,
                ),
                Text(this.informationModel.name),
                FutureBuilder(
                  future: this.showLastLocationInformation(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Column(
                        children: snapshot.data,
                      );
                    }
                    return Spacer();
                  },
                ),
                ElevatedButton(
                  child: Text("Borrar mis datos"),
                  onPressed: this.deleteInformation,
                )
              ],
            ),
          ),
        );
      } else {
        return Container(
          child: Column(
            children: [
              Icon(
                Icons.account_circle,
                color: Colors.green[200],
                size: 200,
              ),
              Text(
                "Para escanear su código QR debe ingresar sus datos",
                style: TextStyle(
                  color: Colors.red[600],
                  fontWeight: FontWeight.bold,
                ),
              ),
              ElevatedButton(
                child: Text("Comenzar ahora"),
                onPressed: () => this.changeToForm(),
              )
            ],
          ),
        );
      }
    } else {
      return CircularProgressIndicator();
    }
  }

  FloatingActionButton fab(BuildContext buildContext) {
    return FloatingActionButton(
      child: Icon(
        Icons.camera_alt,
      ),
      onPressed: () => {
        if (this.informationModel == null)
          {
            () {
              alertDialog(context, "No puede escanear un código", "Primero debe llenar sus datos personales");
            }
          }
        else
          {
            _scan(buildContext),
          }
      },
    );
  }

  createEvent(buttonContext, String qrData) async {
    try {
      Map<String, dynamic> qrMap = jsonDecode(qrData);
      Map<String, dynamic> newEventMap = {
        "device_id": this._deviceId,
        "establishment_id": qrMap["establishment_id"],
        "qr_date": qrMap["qr_date"],
      };
      Event newEvent = await EventAdapter.create(newEventMap);
      if (newEvent != null) {
        // Success
        User eventUser = await UserAdapter.getOneUser(newEvent.userId);
        alertDialog(
          context,
          "Todo bien",
          "Evento registrado en: " + eventUser.name,
        );
        this.updateLastLocation(eventUser, new DateTime.now());
      } else {
        Scaffold.of(buttonContext).showSnackBar(
          SnackBar(
            content: Text("Hubo un error, inténtelo más tarde"),
          ),
        );
        alertDialog(context, "Todo mal", "Solo eso");
      }
    } catch (ex) {
      Scaffold.of(buttonContext).showSnackBar(
        SnackBar(
          content: Text("Hubo un error, inténtelo más tarde"),
        ),
      );
      alertDialog(context, "Todo mal", ex.toString());
      throw ex;
    }
  }

  alertDialog(context, String title, String content) {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: Text(title),
        content: Text(content),
        actions: [
          FlatButton(
            child: Text("Ok"),
            onPressed: () {
              Navigator.pop(context, false);
            },
          ),
        ],
      ),
    );
  }

  Future<void> _scan(buildContext) async {
    ScanResult codeScanner = await BarcodeScanner.scan();
    String rawData = codeScanner.rawContent;
    if (rawData != "") {
      createEvent(buildContext, rawData);
    }
  }

  void deleteInformation() async {
    await InformationAdapter.delete(this.informationModel);
    Navigator.pushReplacementNamed(context, "/client");
    storage.delete(key: "last_location");
    storage.delete(key: "arrival_date");
  }

  void updateLastLocation(User location, DateTime arrivalDate) async {
    this.setState(() {
      this._currentLocation = location;
      this._arrivalDate = new DateTime.now();
    });
    storage.write(
      key: "last_location",
      value: this._currentLocation.id.toString(),
    );
    storage.write(
      key: "arrival_date",
      value: this._arrivalDate.toString(),
    );
  }

  Future<List<Widget>> showLastLocationInformation() async {
    try {
      // Get last event
      Event lastEvent = (await EventAdapter.getLastFromInformation(this._deviceId));
      User lastLocation = await UserAdapter.getOneUser(lastEvent.userId);
      int daysSinceArrival = DateTime.now().difference(lastEvent.date).inDays;
      // TODO - si la visita es de hoy, mostrar algo distinto como si estuviera en el bar
      List<Widget> returnedList;
      if (daysSinceArrival < 0) return [];
      if (daysSinceArrival == 0) {
        returnedList = [
          Text("Última ubicación visitada: ${lastLocation.name}, hoy"),
        ];
      } else {
        returnedList = [
          Text("Última ubicación visitada: "),
          Text(lastLocation.name),
          Text("Hace " + daysSinceArrival.toString() + " días"),
        ];
      }
      if (lastLocation.carta != null) {
        returnedList.add(
          ElevatedButton(
            child: Text("Ver la carta"),
            onPressed: () {
              this.changeRoute("pdf", arguments: lastLocation.carta);
            },
          ),
        );
      }
      return returnedList;
    } catch (e) {
      return [];
    }
  }
}

class DeviceInfo {
  final String deviceId;

  DeviceInfo(this.deviceId);
}
