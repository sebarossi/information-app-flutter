import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:information_app/index.dart';
import 'package:information_app/static/help.dart';

class HelpView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ayuda"),
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
        child: homePage(context),
      ),
    );
  }

  void goBack(context) {
    Navigator.pop(context);
  }

  Widget homePage(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(5, 20, 10, 0),
            child: Column(
              children: [
                Text(
                  "Funcionamiento general",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  GENERAL_EXPLAINATION,
                  textAlign: TextAlign.justify,
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(5, 20, 10, 0),
            child: Column(
              children: [
                Text(
                  "Captura de datos",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  DATA_COLLECTION,
                  textAlign: TextAlign.justify,
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(5, 20, 10, 0),
            child: Column(
              children: [
                Text(
                  "Notificaciones por alertas",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  ALERT_NOTIFICATIONS,
                  textAlign: TextAlign.justify,
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(5, 20, 10, 0),
            child: Column(
              children: [
                Text(
                  "Privacidad de los clientes",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  CLIENTS_PRIVACY,
                  textAlign: TextAlign.justify,
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(5, 20, 10, 50),
            child: Column(
              children: [
                Text(
                  "Borrado de datos",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  DATA_DELETEMENT,
                  textAlign: TextAlign.justify,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<String> loadText(BuildContext context, String filename) async {
    return await DefaultAssetBundle.of(context)
        .loadString('assets/text/' + filename);
  }
}
