import 'dart:convert';
import 'package:information_app/model/base_model.dart';

import 'base_adapter.dart';
import 'package:information_app/model/event_model.dart';

class EventAdapter extends Adapter {
  static Future<Event> getOneEvent(int id) {
    return Adapter.getOne<Event>("event", id, "event");
  }

  static Future<List<Event>> getAllEvents() {
    return Adapter.getAll<Event>("event", "events");
  }

  static Future<Event> create(Map<String, dynamic> newEvent) {
    return Adapter.create<Event>("event", newEvent, "event");
  }

  static Future<Map<String, dynamic>> sendAlert(
      int userId, DateTime alertDate, String alertTitle, String alertMessage) async {
    Map<String, dynamic> body = {
      "userId": userId,
      "alertDate": alertDate.toString(),
    };
    if (alertMessage != null) {
      body["alertTitle"] = alertTitle;
      body["alertMessage"] = alertMessage;
    }
    final resp = await Adapter.httpPost(Adapter.actionUrl("event", "notification"), body: json.encode(body));
    dynamic response = json.decode(resp.body);
    if (response["success"] == true) {
      return response["data"];
    } else {
      throw response["message"];
    }
  }

  static Future<Event> getLastFromInformation(id) async {
    final resp = await Adapter.httpGet(Adapter.actionUrl("event", "lastFromInfo/" + id.toString()));
    dynamic response = json.decode(resp.body);
    if (response["success"] == true) {
      return BaseModel.oneFromJson<Event>(response["data"]);
    }
    throw Exception(response["message"]);
  }
}
