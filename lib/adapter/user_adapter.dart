import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:information_app/model/base_model.dart';
import 'package:path_provider/path_provider.dart';
import 'base_adapter.dart';
import 'package:information_app/model/user_model.dart';

class UserAdapter extends Adapter {
  static Future<User> getOneUser(int id) {
    return Adapter.getOne<User>("user", id, "user");
  }

  static Future<List<User>> getAllUsers() {
    return Adapter.getAll<User>("user", "users");
  }

  static Future<Map<String, dynamic>> getInfo() async {
    final resp = await Adapter.httpGet(Adapter.actionUrl("user", "info"));
    dynamic response = json.decode(resp.body);
    if (response["success"] == true) {
      return {
        "login": response["data"]["login"],
        "user": response["data"]["user"] != null
            ? BaseModel.oneFromJson<User>(
                response["data"]["user"],
              )
            : null,
      };
    }
    throw Exception(response["message"]);
  }

  static Future<Map<String, dynamic>> login(
      Map<String, String> loginMap) async {
    final resp = await Adapter.httpPost(
      Adapter.actionUrl("user", "login"),
      body: jsonEncode(loginMap),
    );
    dynamic response = json.decode(resp.body);
    if (response["success"] == true) {
      User loggedUser = BaseModel.oneFromJson<User>(response["data"]["user"]);
      return {"user": loggedUser, "token": response["data"]["token"]};
    } else {
      throw Exception(response["message"]);
    }
  }

  static Future<String> upload(int userId, File file) async {
    final resp = await Adapter.httpPostImage(
        Adapter.actionUrl("user", "upload"), "carta", file,
        fields: {"id": userId.toString()});
    dynamic response = json.decode(resp.body);
    if (response["success"] == true) {
      return response["data"]["filePath"];
    } else {
      throw response["message"];
    }
  }

  static Future<File> getFile(String filePath) async {
    Map<String, String> body = {"filePath": filePath};
    final resp = await Adapter.httpPost(Adapter.actionUrl("user", "file"),
        body: json.encode(body));
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = new File('$dir/tmp_carta.pdf');
    await file.writeAsBytes(resp.bodyBytes);
    return file;
    // dynamic response = json.decode(resp.body);
    // if (response["success"] == true) {
    //   return response["data"]["file"];
    // } else {
    //   throw response["message"];
    // }
  }
}
