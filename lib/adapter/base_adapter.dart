import 'dart:convert';
import 'package:information_app/index.dart';
import 'package:http/http.dart' as http;
import 'package:information_app/model/base_model.dart';
import 'package:http_parser/http_parser.dart';

final storage = FlutterSecureStorage();

class Adapter {
  static final protocol = "http";
  static final pageName = "192.168.0.99";
  static final port = 3000;
  static final apiRoute = "api";
  static final developer = true;

  static final entityActionMap = {
    "user": "user",
    "event": "event",
    "information": "information",
  };

  static String getBaseUrl() {
    return developer
        ? protocol + "://" + pageName + ":" + port.toString() + "/" + apiRoute + "/"
        : "http://information-app-test.herokuapp.com/" + apiRoute + "/";
  }

  static String getUrl(resource, action) {
    return getBaseUrl() + resource + "/" + action;
  }

  static actionUrl(String entity, String action) {
    String url = entityActionMap[entity];
    return Adapter.getUrl(url, action);
  }

  static Future<Response> httpGet(String url) async {
    String jwt = await storage.read(key: "jwt");
    jwt = jwt == null ? "" : jwt;
    final resp = http.get(url, headers: {HttpHeaders.authorizationHeader: "Bearer " + jwt});
    return resp;
  }

  static Future<Response> httpPost(String url, {body}) async {
    String jwt = await storage.read(key: "jwt");
    jwt = jwt == null ? "" : jwt;
    final resp = http.post(
      url,
      body: body,
      headers: {HttpHeaders.authorizationHeader: "Bearer " + jwt, "content-type": "application/json"},
    );
    return resp;
  }

  static Future<Response> httpPut(String url, {body}) async {
    String jwt = await storage.read(key: "jwt");
    jwt = jwt == null ? "" : jwt;
    final resp = http.put(
      url,
      body: body,
      headers: {HttpHeaders.authorizationHeader: "Bearer " + jwt, "content-type": "application/json"},
    );
    return resp;
  }

  static Future<Response> httpDelete(String url, {body}) async {
    String jwt = await storage.read(key: "jwt");
    jwt = jwt == null ? "" : jwt;
    final resp = http.delete(
      url,
      headers: {HttpHeaders.authorizationHeader: "Bearer " + jwt, "content-type": "application/json"},
    );
    return resp;
  }

  static Future<Response> httpPostImage(String url, String fileFieldName, File file,
      {Map<String, String> fields}) async {
    // REQUEST
    var request = http.MultipartRequest(
      'POST',
      Uri.parse(url),
    );

    // HEADERS
    String jwt = await storage.read(key: "jwt");
    jwt = jwt == null ? "" : jwt;
    Map<String, String> headers = {
      HttpHeaders.authorizationHeader: "Bearer " + jwt,
      "content-type": "multipart/form-data"
    };
    request.headers.addAll(headers);

    // FILE
    request.files.add(
      http.MultipartFile(fileFieldName, file.readAsBytes().asStream(), file.lengthSync(),
          filename: "tmp.pdf", contentType: MediaType('application', 'pdf')),
    );
    if (fields != null) request.fields.addAll(fields);

    final resp = await request.send();
    return await http.Response.fromStream(resp);
  }

  static Future<List<X>> getAll<X>(String entity, String responseField) async {
    final resp = await Adapter.httpPost(actionUrl(entity, ""));
    dynamic response = json.decode(resp.body);
    if (response["success"] == true) {
      List<X> entities = BaseModel.listFromJson<X>(response["data"][responseField]);
      return entities;
    } else {
      throw Exception(response["message"]);
    }
  }

  static Future<X> getOne<X>(String entity, id, String responseField) async {
    final resp = await Adapter.httpGet(actionUrl(entity, id.toString()));
    dynamic response = json.decode(resp.body);
    if (response["success"] == true) {
      X entity = BaseModel.oneFromJson<X>(
        response["data"][responseField],
      );
      return entity;
    } else {
      throw Exception(response["message"]);
    }
  }

  static Future<X> create<X>(String entity, Map<String, dynamic> newEntity, String responseField) async {
    final resp = await Adapter.httpPost(actionUrl(entity, ""), body: json.encode(newEntity).toString());
    dynamic response = json.decode(resp.body);
    if (response["success"] == true) {
      X entity = BaseModel.oneFromJson<X>(response["data"][responseField]);
      return entity;
    } else {
      throw Exception(response["message"]);
    }
  }

  static Future<bool> delete<X>(String entityName, String entityId, String idField) async {
    final resp = await Adapter.httpDelete(actionUrl(entityName, ""), body: json.encode({idField: entityId}));
    dynamic response = json.decode(resp.body);
    if (response["success"] == true) {
      return true;
    } else {
      throw Exception(response["message"]);
    }
  }
}
