import 'base_adapter.dart';
import 'package:information_app/model/information_model.dart';

class InformationAdapter extends Adapter {
  static Future<Information> getOneInformation(id) {
    return Adapter.getOne<Information>("information", id, "information");
  }

  static Future<List<Information>> getAllInformations() {
    return Adapter.getAll<Information>("information", "informations");
  }

  static Future<Information> create(Map<String, String> newInformation) {
    return Adapter.create<Information>("information", newInformation, "information");
  }

  static Future<bool> delete(Information information) {
    return Adapter.delete<Information>("information", information.deviceId, "device_id");
  }
}
